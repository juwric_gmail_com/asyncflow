import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamFormComponent } from './team-form/team-form.component';
import { SaveGuard } from '../core/save.guard';

const routes: Routes = [
  {
    path: '',
    component: TeamListComponent,
  },
  {
    path: 'edit/:id',
    component: TeamFormComponent,
    canDeactivate: [SaveGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [SaveGuard],
  exports: [RouterModule],
})
export class TeamRoutingModule {}
