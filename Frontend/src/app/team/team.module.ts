import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamFormComponent } from './team-form/team-form.component';
import { TeamRoutingModule } from './team-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormatPipe } from '../core/format.pipe';

@NgModule({
  declarations: [TeamListComponent, TeamFormComponent, FormatPipe],
  imports: [CommonModule, TeamRoutingModule, FormsModule, ReactiveFormsModule],
})
export class TeamModule {}
