import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskFormComponent } from './task-form/task-form.component';
import { TaskRoutingModule } from './task-routing.module';
import { BoldDirective } from '../core/bold.directive';

@NgModule({
  declarations: [TaskListComponent, TaskFormComponent, BoldDirective],
  imports: [CommonModule, TaskRoutingModule],
})
export class TaskModule {}
