import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ITask, TaskState } from 'src/app/core/models/ITask';
import { TaskService } from 'src/app/core/services/task.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent implements OnInit {
  tasks: ITask[] = [];
  constructor(private taskService: TaskService, private router: Router) {}

  ngOnInit(): void {
    this.taskService.findAll().subscribe((res) => {
      this.tasks = res;
    });
  }

  editTeam(taskId: number): void {
    this.router.navigate(['tasks', 'edit', taskId]);
  }

  deleteTeam(taskId: number): void {
    this.taskService.delete(taskId).subscribe((res) => {
      this.tasks = this.tasks.filter((t) => t.id != taskId);
    });
  }

  enumName(name: TaskState): any {
    return TaskState[name];
  }
}
