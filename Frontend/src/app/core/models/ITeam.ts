import { IBase } from './IBase';

export interface ITeam extends IBase {
  name: string;
  createdAt: string;
}
