import { IBase } from './IBase';
import { ITask } from './ITask';
import { ITeam } from './ITeam';
import { IUser } from './IUser';

export interface IProject extends IBase {
  name: string;
  description: string;
  deadline: string;
  createdAt: string;
  authorId: number;
  author: IUser;
  teamId: number;
  team: ITeam;
  tasks: ITask[];
}
