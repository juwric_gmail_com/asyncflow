import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ITask } from '../models/ITask';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  constructor(private apiService: ApiService) {}

  findAll(): Observable<any> {
    return this.apiService.get('/tasks');
  }

  update(id: number, task: ITask): Observable<any> {
    return this.apiService.put(`/tasks`, { ...task, id });
  }

  findOne(taskId: number): Observable<any> {
    return this.apiService.get(`/tasks/${taskId}`);
  }

  delete(taskId: number): Observable<any> {
    return this.apiService.delete(`/tasks/${taskId}`);
  }
}
