import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ITeam } from '../models/ITeam';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class TeamService {
  constructor(private apiService: ApiService) {}

  findAll(): Observable<any> {
    return this.apiService.get('/teams');
  }

  update(id: number, team: ITeam): Observable<any> {
    return this.apiService.put(`/teams`, { ...team, id });
  }

  findOne(teamId: number): Observable<any> {
    return this.apiService.get(`/teams/${teamId}`);
  }

  delete(teamId: number): Observable<any> {
    return this.apiService.delete(`/teams/${teamId}`);
  }
}
