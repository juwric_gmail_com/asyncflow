﻿using AutoMapper;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.Common.DTOs.Project;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : BaseService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IMapper mapper, IProjectRepository projectRepository) : base(mapper)
        {
            _projectRepository = projectRepository;
        }

        public async Task<List<ProjectDTO>> GetAll()
        {
            var teams = await _projectRepository.GetAll();
            return _mapper.Map<List<ProjectDTO>>(teams);
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var user = await _projectRepository.GetById(id);
            if (user == null)
            {
                throw new Exception("The project is not found");
            }

            return _mapper.Map<ProjectDTO>(user);
        }

        public async Task<ProjectDTO> CreateProject(ProjectCreateDTO projectCreateDTO)
        {
            var teamEntity = _mapper.Map<Project>(projectCreateDTO);
            //teamEntity.Id = (_projectRepository.GetAll().Max(u => u.Id)) + 1;
            teamEntity.CreatedAt = DateTime.Now;
            await _projectRepository.Add(teamEntity);

            return _mapper.Map<ProjectDTO>(teamEntity);
        }

        public async System.Threading.Tasks.Task UpdateProject(ProjectUpdateDTO projectUpdateDTO)
        {
            var projectEntity = await GetProjectById(projectUpdateDTO.Id);
            var updatedProjectEntity = _mapper.Map<Project>(projectUpdateDTO);
            updatedProjectEntity.CreatedAt = projectEntity.CreatedAt;
            updatedProjectEntity.Deadline = projectEntity.Deadline;

            await _projectRepository.Update(updatedProjectEntity);
        }

        public async System.Threading.Tasks.Task DeleteProject(int id)
        {
            await GetProjectById(id);
            await _projectRepository.Delete(id);
        }
    }
}
