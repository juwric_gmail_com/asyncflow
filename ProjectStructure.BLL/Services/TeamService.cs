﻿using AutoMapper;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.Common.DTOs.Team;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : BaseService
    {
        private readonly ITeamRepository _teamRepository;

        public TeamService(IMapper mapper, ITeamRepository teamRepository) : base(mapper)
        {
            _teamRepository = teamRepository;
        }

        public async Task<List<TeamDTO>> GetAll()
        {
            var teams = await _teamRepository.GetAll();
            return _mapper.Map<List<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> GetUserById(int id)
        {
            var user = await _teamRepository.GetById(id);
            if (user == null)
            {
                throw new Exception("The team is not found");
            }

            return _mapper.Map<TeamDTO>(user);
        }

        public async Task<TeamDTO> CreateUser(TeamCreateDTO teamCreateDTO)
        {
            var teamEntity = _mapper.Map<Team>(teamCreateDTO);
            //teamEntity.Id = (_teamRepository.GetAll().Max(u => u.Id)) + 1;
            teamEntity.CreatedAt = DateTime.Now;
            await _teamRepository.Add(teamEntity);

            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public async System.Threading.Tasks.Task UpdateUser(TeamUpdateDTO teamUpdateDTO)
        {
            var teamEntity = await GetUserById(teamUpdateDTO.Id);
            var updatedTeamEntity = _mapper.Map<Team>(teamUpdateDTO);
            updatedTeamEntity.CreatedAt = teamEntity.CreatedAt;

            await _teamRepository.Update(updatedTeamEntity);
        }

        public async System.Threading.Tasks.Task DeleteUser(int id)
        {
            await GetUserById(id);
            await _teamRepository.Delete(id);
        }
    }
}
