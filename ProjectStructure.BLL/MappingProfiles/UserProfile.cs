﻿using AutoMapper;
using ProjectStructure.Common.DTOs.User;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserCreateDTO, User>();
            CreateMap<UserUpdateDTO, User>();
        }
    }
}
