﻿using CollectionsLINQ;
using CollectionsLINQ.Models;
using System;
using System.Collections.Generic;

namespace ProjectStructure.Client
{
    public class Program
    {
        static IEnumerable<Project> list;

        public static async System.Threading.Tasks.Task Main(string[] args)
        {
            MappingService service = new MappingService("https://localhost:5001");
            await service.Init();
            list = service.GetProjectList();

            Queries queries = new Queries(list, service);
            var req1 = queries.GetTasksSpecificUser();
            var req2 = queries.GetTasksAssigned();
            var req3 = queries.GetListTasksCompleted();
            var req4 = queries.GetListWihtIdTeamName();
            var req5 = queries.GetListOfUsersAlphabetically();
            var req6 = queries.GetStructure1();
            var req7 = queries.GetStructure2();

            var markedTaskId = await queries.MarkRandomTaskWithDelay(1000);
            Console.WriteLine($"MarkedTaskId = {markedTaskId}");

            Console.WriteLine("........");
            Console.ReadKey();
        }
    }
}
