﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTOs.Project;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.Presentation.Controllers
{
    public class ProjectsController : BaseController
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProjectDTO>>> Get()
        {
            return Ok(await _projectService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetById(int id)
        {
            return Ok(await _projectService.GetProjectById(id));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProject([FromBody] ProjectUpdateDTO project)
        {
            await _projectService.UpdateProject(project);
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> CreateProject([FromBody] ProjectCreateDTO project)
        {
            await _projectService.CreateProject(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}
