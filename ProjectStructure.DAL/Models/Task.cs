﻿using ProjectStructure.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.DAL.Models
{
    public class Task : BaseEntity
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }

        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int? PerformerId { get; set; }
        public User Performer { get; set; }
    }
}
