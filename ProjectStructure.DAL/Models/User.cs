﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.DAL.Models
{
    public class User : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public string Website { get; set; }
        public string Company { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }
    }
}
