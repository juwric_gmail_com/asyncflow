﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.DAL
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Project> Projects { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<User> Users { get; private set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        { }
    }
}
