﻿using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.Contracts
{
    public interface IProjectRepository : IBaseRepository<Project>
    {
    }
}
