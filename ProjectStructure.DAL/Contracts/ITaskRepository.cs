﻿using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.Contracts
{
    public interface ITaskRepository : IBaseRepository<Task>
    {
    }
}
