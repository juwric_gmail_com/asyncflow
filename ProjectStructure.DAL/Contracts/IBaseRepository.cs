﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Contracts
{
    public interface IBaseRepository<T> where T : class
    {
        ValueTask<T> GetById(int id);
        Task<List<T>> GetAll();

        Task Add(T entity);
        Task Update(T entity);
        Task Delete(int id);
    }
}
