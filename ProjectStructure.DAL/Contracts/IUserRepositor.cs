﻿using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.Contracts
{
    public interface IUserRepository : IBaseRepository<User>
    {
    }
}
